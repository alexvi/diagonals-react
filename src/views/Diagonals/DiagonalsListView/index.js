import React from 'react';
import {Container, Grid, CssBaseline, Typography, makeStyles, Grow} from '@material-ui/core';
import DiagonalCard from './DiagonalCard';

const useStyles = makeStyles((theme) => ({
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(3, 3, 2),
  },
  letter: {
    fontFamily : 'Cominc-Sans',
    color: '#e91e63'
  }, 
  cardGrid: {
    backgroundColor: '#fce4ec'
  }
}));

const DiagonalList = (data) => {
  const classes = useStyles();
  return (
    <React.Fragment>
      <CssBaseline />
      <Grow in={true} timeout={1000}>
      <main>
        {/* Hero unit */}
        <div className={classes.heroContent}>
          <Container maxWidth="lg">
            <Typography className={classes.letter} component="h1" variant="h3" align="center" color="textPrimary" gutterBottom>
              Patrones de movimiento en diagonal
            </Typography>
            <Typography variant="h5" align="center" color="textSecondary" paragraph>
              Selecciona una diagonal 
            </Typography>
          </Container>
        </div>
        <Container className={classes.cardGrid} maxWidth="md">
          {/* End hero unit */}
          <Grid container spacing={4}>
            {data.data.map((object) => (
              <Grid item key={object.id} xs={12} sm={6} md={4}>
                <DiagonalCard diagonal={object}/>
              </Grid>
            ))}
          </Grid>
        </Container>
      </main>
     
    </Grow>
    </React.Fragment>
  );
}

export default DiagonalList