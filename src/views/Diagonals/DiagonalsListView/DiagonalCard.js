import React, { useState } from 'react';
import {Card, CardMedia, CardContent, CardActionArea, Typography, makeStyles, Box, Grow} from '@material-ui/core';
import { Link as RouterLink } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({

    cardGrid: {
      paddingTop: theme.spacing(8),
      paddingBottom: theme.spacing(8),
    },
    card: {
      height: '100%',
      display: 'flex',
      flexDirection: 'column',
      backgroundColor: '#ec407a'
    },
    cardMedia: {
      paddingTop: '56.25%', // 16:9  
    },
    cardContent: {
      flexGrow: 1,
    }, 
    title: {
      color : 'white'
    }
  }));

const DiagonalCard = (object) =>{

  const classes = useStyles();    
  const diagonal = object.diagonal;
  const link = '/diagonal/'+diagonal.id;  
  console.log(diagonal);
    return(
      <RouterLink style={{ textDecoration: 'none' }} to={link}>          
        <Grow in={true} timeout={1500}>
        <Box boxShadow={3}>
          <Card className={classes.card}>
            <CardActionArea>
                <CardMedia
                    className={classes.cardMedia}
                    image= {diagonal.image}
                    title="Diagonal"
                />
                <CardContent className={classes.cardContent}>
                    <Typography className={classes.title} gutterBottom variant="h5" component="h2" >
                        {diagonal.name}
                    </Typography>
                </CardContent>
              </CardActionArea>
          </Card>
        </Box>
        </Grow>
      </RouterLink>
    )
}

export default DiagonalCard