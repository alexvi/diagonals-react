import { Button, Container, Typography, makeStyles, Grid, Card, CardMedia, CardContent, CardActions, Box, Grow, List, ListItem, ListItemIcon, ListItemText, Paper, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions } from '@material-ui/core'
import ReplyIcon from '@material-ui/icons/Reply';
import React, { useState } from 'react'
import { Link as RouterLink, useParams } from 'react-router-dom';
import EmojiPeopleIcon from '@material-ui/icons/EmojiPeople';
import cyan from '@material-ui/core/colors/cyan';

const useStyles = makeStyles((theme) => ({
    heroContent: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(3, 0, 2),
      },
      letter: {
        fontFamily : 'Cominc Sans',
        color: '#e91e63',
      },
      cardGrid: {        
        paddingBottom: theme.spacing(0),
        align: 'center',
        alignContent: 'center'
      },
      cardTitle: {
        color: 'white'
      },
      card: {
        display: 'flex',
        flexDirection: 'column',
        height : '235px',
        width: '220px'
      },
      principalImg: {
        display: 'flex',
        flexDirection: 'column',
        height : '500px',
        width: '400px',
        backgroundColor: '#f06292',
      },
      cardContent: {
        flexGrow: 1,
      },
      root: {
        minWidth: 275,
        heigth: '500px'
      },
      bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
      },
      title: {
        fontSize: 14,
      },
      pos: {
        marginBottom: 12,
        color: 'white'
      },
      Content:{
        paddingBottom: 0
      },
      Actions:{
        paddingTop: 0
      },
      List:{
        width: '350px',
      }
  }));

const DiagonalInfo = (data) =>{
  const [open, setOpen] = useState(false);
  const [modalState, setModalState] = useState(0);
    let { id } = useParams();
    const classes = useStyles();
    const diagonal = data.data.find( diagonal => diagonal.id ==id);
    const components = diagonal.components;
    const handleOpen = (id) => {
      setModalState(id);
      setOpen(true);
    };
    const handleClose = () => {
      setModalState(0);
      setOpen(false);
    }
    return(
        <Grow in={true} timeout={1000}>
        <main >     
            <div className={classes.heroContent}>
                    <Typography className={classes.letter} component="h1" variant="h3" align="center" gutterBottom>
                    {diagonal.name}
                    </Typography>
            </div>
            <RouterLink className={classes.backButton} style={{ textDecoration: 'none' }} to='/'>
                    <Button variant="contained" color="secondary" startIcon={<ReplyIcon/>}>Volver</Button>
            </RouterLink>
            <Container className={classes.cardGrid} maxWidth="md">
            {/* End hero unit */}
            <Grid container spacing={5} >
                <Grid item >
                <Grow in={true} timeout={2000}>
                    <Box boxShadow={3}>
                    <CardMedia
                                className={classes.principalImg}
                                image= {diagonal.image}
                                title={diagonal.name}
                            />       
                    </Box>
                </Grow>
                </Grid>
                <Grid item>
                <Grow in={true} timeout={3000}>
                    <Box boxShadow={3}>
                    <Card className={classes.principalImg}>
                        <CardContent className={classes.Content}>
                            <Typography className={classes.cardTitle} variant="h5" component="h2">
                            <b>Componentes de la diagonal:</b>
                            </Typography>
                            {/*<Typography className={classes.pos} color="textSecondary">
                            Selecciona un componente para visualizar
                            </Typography>*/}
                        </CardContent>
                        <CardActions className={classes.Actions}>
                          <Paper style={{maxHeight: 400,overflow: 'auto'}}>
                            <List className={classes.List}>
                            {
                                components.map(Element => (<>
                                <ListItem style={{color: '#f48fb1'}}>
                                    <Button key={Element.id}
                                      style={{width: '300px', justifyContent: 'end'}}
                                      variant="contained"
                                      color="secondary"
                                      size="lg"                                     
                                      startIcon={<EmojiPeopleIcon />}
                                    >
                                    {Element.name}
                                    </Button>
                                  </ListItem>
                                  <ListItem >
                                  <Button key={Element.name}
                                    style={{width: '300px', justifyContent: 'end'}}
                                    variant="outlined"
                                    
                                    size="lg"                                  
                                  >
                                  <List>
                                  {Element.muscle.map(Element=>(                                  
                                    <ListItem>
                                      {Element.name}
                                      <Paper>
                                        <img src={Element.image}></img>
                                      </Paper>
                                    </ListItem>
                                  ))}
                                  </List>
                                  </Button>
                                </ListItem>
                                  </>
                                ))
                            }
                            </List>
                            </Paper>
                        </CardActions>
                        </Card>
                        </Box>
                        </Grow>
                </Grid>
            </Grid>
            </Container>
            
        </main>
        </Grow>
    )
}

export default DiagonalInfo