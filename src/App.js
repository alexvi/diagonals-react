import React from 'react';
import { BrowserRouter as Router, Switch, Route} from "react-router-dom";
import Header from './layout/header';
import Footer from './layout/footer';
import DiagonalList from './views/Diagonals/DiagonalsListView';
import DiagonalInfo from './views/Diagonals/DiagonalInfoView';
import Data from './data/Data.json';

const App = () => {
  return (
    <>
    <Header/>
    <Router>
      <Switch>
          <Route exact path="/">
            <DiagonalList data={Data}/>               
          </Route>
          <Route path="/diagonal/:id">
            <DiagonalInfo data={Data}/>
          </Route>
      </Switch>
    </Router>
    <Footer/>
    </>
  );
};

export default App;
