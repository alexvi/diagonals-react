import React from 'react';
import {AppBar, Toolbar, Typography} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import DirectionsRunIcon from '@material-ui/icons/DirectionsRun';
import pink from '@material-ui/core/colors/pink'


const useStyles = makeStyles((theme) => ({
    root:{
        backgroundColor: '#e91e63',
    },
    icon: {
      marginRight: theme.spacing(2),
    }
  }));

const Header = () => {
    const classes = useStyles();
    return(
        <AppBar className={classes.root} position="relative">
            <Toolbar >
            <DirectionsRunIcon className={classes.icon} />
            <Typography variant="h6" color="inherit" noWrap>
                App de diagonales
            </Typography>
            </Toolbar>
        </AppBar>
    );
}

export default Header;