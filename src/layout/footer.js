import React from 'react';
import {Typography} from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';

function Copyright() {
    return (
      <Typography variant="body2" color="textSecondary" align="center">
        Copyright © 
        {new Date().getFullYear()}
        .
      </Typography>
    );
  }
  
  const useStyles = makeStyles((theme) => ({
      footer : {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(6),
      }
  }));
const Footer = () =>{
    const classes = useStyles();
    return(
        <footer className={classes.footer}>
          <Typography variant="subtitle1" align="center" color="textSecondary" component="p">
            Ft. Adriana Julieth Aranda Zemanate
            </Typography>
            <Typography variant="subtitle1" align="center" color="textSecondary" component="p">
            Docente
            </Typography>
            <Typography variant="subtitle1" align="center" color="textSecondary" component="p">
            Fundación Universitaria Maria Cano
            </Typography>
            <Copyright />
        </footer>
    );
}

export default Footer;